const express = require("express");
const cors = require("cors");
const uuid = require("uuid").v4;
const app = express();

app.use(cors());
app.use(express.static(__dirname + "/public"));

const PORT = 3000;
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

const products = [{
		_id: uuid(),
		title: "Navy blue T-shirt",
		image: `http://localhost:${PORT}/assets/images/1.jpg`,
		price: 12,
		availableSizes: ['S', 'XS']
	},
	{
		_id: uuid(),
		title: "Brown T-shirt",
		image: `http://localhost:${PORT}/assets/images/2.jpg`,
		price: 16,
		availableSizes: ['M', 'L']
	},
	{
		_id: uuid(),
		title: "Yellow T-shirt",
		image: `http://localhost:${PORT}/assets/images/4.jpg`,
		price: 18,
		availableSizes: ['M', 'S', 'XS']
	},
	{
		_id: uuid(),
		title: "Dark Green T-shirt",
		image: `http://localhost:${PORT}/assets/images/5.jpg`,
		price: 15,
		availableSizes: ['M', 'L', 'S', 'XS']
	}
];

app.get("/products", (req, res) => {
	res.status(200).json(products);
});

app.get("/", (req, res) => {
	res.render("index");
});